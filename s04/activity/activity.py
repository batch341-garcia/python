# S04 Activity:
# Create an abstract class called Animal that has the following abstract methods ‘Abstract Methods: ea, t (food)make_sound()
# Create two classes that implements the Animal class called Cat and Dog with each of the following
# properties and methods:
# Properties:
# name, breed, age
# Methods:
# getters and setters, implementation of abstract methods, call()

from abc  import ABC, abstractclassmethod

class Animal(ABC):

	@abstractclassmethod
	def eat(self):
		pass

	@abstractclassmethod
	def make_sound():
		pass

class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# setter
	def eat(self, food):
		self._food = food

	def make_sound(self, sound):
		self._sound = sound

	# getter
	def get_eat(self):
		print(f"Serve me {self._food}")

	def get_make_sound(self):
		print(self._sound)

	def call(self):
		print(f"{self._name}, cpme on!")


class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# setter
	def eat(self, food):
		self._food = food

	def make_sound(self, sound):
		self._sound = sound

	# getter
	def get_eat(self):
		print(f"Eaten {self._food}")

	def get_make_sound(self):
		print(self._sound)

	def call(self):
		print(f"Here {self._name}")


dog = Dog("Isis", "Aspin", 3)
dog.eat("Steak")
dog.make_sound("Bark! Woof! Arf!")
dog.get_eat()
dog.get_make_sound()
dog.call()

cat = Cat("Puss", "Pusang Pinot", 2)
cat.eat("Steak")
cat.make_sound("Miaow! Nyaw! Nyaaaa!")
cat.get_eat()
cat.get_make_sound()
cat.call()
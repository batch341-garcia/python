# Python Lists, Dictionaries, Functionms, and Classes

# [SECTION] Lists
# Lists are similar to JS arrays
# To create a list, the square brackets([]) are used

names = ["John","Paul","George", "Ringo"]
programs = ["developer career", "pi-shape", "Short courses"]
durations = [260, 180, 20]
truth_vaues = [True, False, True, True, False]
print(names)
print(programs)
print(durations)
print(truth_vaues)
sample_lists = ["Apple", 3, False, "Potato", 4, True]
print(sample_lists)

# Getting the list size
# The number of elements in a list can be counted using the len() method
print(len(programs))

# Accessing values
# The index numbers in lists start at 0 and ends at (n-1), where n is the number of elements
# Accessing the first element in the list
print(programs[0])

# Accessing the last element in the list
print(programs[-1])

# Accessing a range of values
# list_name[start_index: end_index]
# Note: The end in index is not included
print(programs[0:2]) # will display index 0 and 1

# [Section] Mini exercise:
    # 1. Create a list of names of 5 students
    # 2. Create a list of grades for the 5 students
    # 3. Use a loop to iterate through the lists printing in the following format:
student_names = ["John", "Petter", "Fred", "Kira", "Carl"]
grades = [90, 89, 74, 88, 87]

# Ms Daisy
count = 0
while count < len(student_names):
	print(f"The grade of {student_names[count]} is {grades[count]}")
	count += 1
# My Solution 
# for x in range(len(student_names)):
# 	x == 0
# 	print(f"The grade of {student_names[x]} is {grades[x]}")
# 	x += 1

# Updating lists
print(f"Current value: {programs[2]}")

# Updating the value
programs[2] = "Short Courses"

# Print the new value 
print(f"New value: {programs[2]}")

# List Manipulation
# Append is for adding list items - append() allows to insert items to a list
programs.append("global")
print(programs)

# Deleting List Items - the "del" keyword can be used to delete elements in the list
durations.append(360)
print(durations)

# Delete the last item on the list
del durations[-1]
print(durations)

# Membership checks - the "in" keyword checks if the element is in the list
print(20 in durations) # True 
print(500 in durations) # False

# Sorting lists - the method sorts the list alphanmerically, ascending by default
names.sort()
print(names)

# Emptying a list - the clear() method is used to empty the content of a list
test_lists = [1, 3, 5, 7, 9]
print(test_lists)
test_lists.clear()
print(test_lists)

# [SECTION] Dictionaries
# Dictionaries are used to store data values in key:value pairs. This is similar to the objects in JS.

person1 = {
	"name" : "Daisy",
	"age" : 28,
	"occupation" : "instructor",
	"isEnrolled" : True,
	"subjects"  : ["Python", "SQL", "Django"]
}

print(person1)
# To get the number of key-pairs in a dictionary, the len() method can be used
print(len(person1))

# Accessing values in a dictionary
# To get the items in a dictionary, the key name can be referred using square brackets([])
print(person1["name"])

# The keys() method will return a list of all the keys in the dictionary
print(person1.keys())

# The values() method will return a list of all the values in the dictionary
print(person1.values())

# The items() method will return each item in a dictionary, as key-value pairs in a list
print(person1.items())


# Adding key-value paris can be done with either putting a new key and assignig a value or the update() method
person1["nationality"] = "Filipino"
print(person1)

person1.update({"fav_food" : "Sinigang"})
print(person1)

# Deleting Entries can be done using the pop() method or the del keyword
person1.pop("fav_food")
print(person1)
del person1["nationality"]
print(person1)

# The clear() method empties a dictionary
person2 = {
	"name" : "John",
	"age" : 18
}

print(person2)
person2.clear()
print(person2)

# Looping through dictionaries
for key in person1:
	print(f"The value of {key} is {person1[key]}")

# Nested distionaries
person3 = {
	"name" : "Monica",
	"age" : 20,
	"occupation" : "poet",
	"isEnrolled" : True,
	"subjects"  : ["Python", "SQL", "Django"]
}

class_room = {
	"student1" : person1,
	"student2" : person3
}

print(class_room)


# [Section] Mini Exercise
    # 1. Create a car dictionary with the following keys:
    # brand, model, year of make, color
    # 2. Print the following statement from the details:
    # "I own a <Brand> <Model> and it was made in <Year of Make>"

car = {
 	"brand" : "Tesla",
 	"model" : "Model Y",
 	"yearOfMake" : 2023,
 	"color" : "White"
 }

print(f"I own a {car['brand']} {car['model']} and it was made in {car['yearOfMake']}")

print("----------------")
# person1["subjects"].pop()
# print(person1["subjects"])
# person1["subjects"].append("JS")
# print(person1["subjects"])
# person1["subjects"].remove("SQL")
# print(person1["subjects"])

# for keyIndex in car:
# 	print(f"The value of {keyIndex} is {car[keyIndex]}")

# [SECTION] Functions
# Functions are blocks of code then run when called

# "def" keyword is used to create functions
# def <function name>()

def my_greeting():
	# Code to be run when my_greeting is called
	print("Hello, User!")

# Calling/Invoking a function - just specify the function name and provide the values.
my_greeting()

# Parameters can be added to functions to have more control to what the inputs for if ever needed
def greet_user(username):
	print(f"Hello, {username}!")

# Arguments are the values that are provided to the function/ substituted to the parameters
greet_user("Bob")
greet_user("Amy")

# Return stament - the "return" keyword

def addition(num1, num2):
	return num1 + num2

sum = addition(5, 10)

print(f"The sum is {sum}")

# [SECTION] Lambda functions
# A lambda function is a small, anonymous function that can be used for callbacks

greeting = lambda person : f"Hello, {person}"
print(greeting("Elsie"))
print(greeting("Anthony"))

mult = lambda a, b : a * b
print(mult(5, 6))
print(mult(6, 99))

# Mini exercise:
    # Create a function that gets the square of a number

def squared (a):
	sqr = a * a
	return f"The squared of {a} is: {sqr}" 

square = squared(2)
print(square)

# [SECTION] Classes
# Classes serve as blueprint to describe the concept of objects
# Each object has characteristics(properties) and behaviors(methods)
# To create a class, the "class" keyword is used along with the class name that start with an Uppercase letter 
class Car():
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		self.fuel = "Gasoline"
		self.fuel_level = 0
	# methods
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print('filling up the fuel tank...')
		self.fuel_level = 100
		print(f"New fuel_level: {self.fuel_level}")

	def drive(self, distance):
		print(f"The car has driven {distance} kilometers")
		print(f"The car's fuel level is {self.fuel_level - distance}")

# Creating a new instance is done by calling the class and providing the arguments
new_car = Car("Nissan", "GT-R", "2019")

# Displaying attributes can be done using dot notation
print(f"My car is a {new_car.brand} {new_car.model} and year make is: {new_car.year_of_make}")

# Calling methods of the instance
new_car.fill_fuel()


# Mini Exercise
    # 1. Add a method called drive with a parameter called distance
    # 2. The method would output 2 things
    # "The car has driven <distance> kilometers"
    # "The car's fuel level is <fuel level - distance>"



new_car.drive(15)
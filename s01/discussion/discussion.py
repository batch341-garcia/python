# [SECTION] Comments
# Comments in python are done using the "#" symbol

# [SECTION] Python Syntax

print("Hello World!");

# [SECTION] Indentation
# Indentation in Python is very important
# In Pythin, indentation is used to indicate a block of code

# [SECTION] Variabls
# The terminology used  for variable names is "identifier"
# In Python, a variable is decalred by stating the variabl name and assigning a value using the equality symbol

# [SECTION] Naming convention
# In Python uses the Snake case convention

age = 35
print(age)
# snake casing
middle_initial = "C"
print(middle_initial)

# Python allows assigning of values to multiple variables in one line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name4)

# [SECTION] Data Types
# 1. Strings
full_name = "John Doe"
print(full_name)

secret_code = 'Pas$$word'
print(secret_code)

# 2. Numbers(int, float, complex) - for integers, decimals, and complex numbers
num_of_days = 365 # thi is an integer
print(num_of_days)

pi_approx = 3.1416 # This is a decimal
print(pi_approx)

complex_num = 1 + 5j # this is a complex number
print(complex_num)
print(complex_num.real)
print(complex_num.imag)

# Booleans - for turth values

is_learning = True
is_difficult = False

print(is_learning)
print(is_difficult)

# [SECTION] Using Variables
# Just like in JS, variable are used by simply calling the name of the indentifies
# To use variables, concatenation ("+") symbol between strings can be used
print("My name is " + full_name)
# This returns a "typeError" as numbers can't be concaneted to strings
# print("My age is " + age)

print("My age is " + str(age))

# [SECTION] Typecastinig
# There may be times when we want to specify a tyype on to a variable. This can be done with casting. Here are some function that can be used:
# 1. int() - converts the value into an integer value
# 2. float() - converts the value into an integer value
# 3. str() - converts the value into strings

print(int(3.5))
print(float(3))

# Another way to avoid  the type error in printing w/o the use of typecasting is the use of F=string
# To use F-string, add a lowercase "f" before the string and place the desired variable in {}
print(f"Hi, my name is {full_name} and my age is {age}")

# [SECTION] Operations
# Python has operator families taht can be used  manipulate variables
# Arithmetic operators - perform mathical operations
print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4)
print(2 ** 6)

# Assignment operations - used to assign values to variables
num1 = 3
print(num1)
num1 += 4
print(num1)

# Other assignment opeators (-=, *=, /=, 

# Comparisson operators - used to compare values (returns a boolean value)
print(1 == 1) # True
print(1 == "1") # False

# other operators ( !=, >=, <=, >, <)

# Logical operators - used to combine cindition statements
print (True and False)
print(not False)
print(False or True)
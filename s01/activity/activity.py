# [SECTION]
# 1.A
name = "Jose"
age = 38
occupation = "writer"
movie = "One More Chance"
rating = 99.6

print("I am " + name + ", " + "and I am " + str(age) + " years old, I work as a " + occupation + " , and my rating for " + movie + " is " + str(rating) + " %")

# 2
num1 = 10
num2 = 15
num3 = 20

# 2.A
print(num1 * num2)

# 2.B
print(num1 < num3)

#3.C
print(num3 + num2)
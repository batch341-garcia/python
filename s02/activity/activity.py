# S02 Activity:
# 1. Get a year from the user and determine if it is a leap year or not.
# 2.  Get two numbers (row and col) from the user and create by row x col grid of asterisks given the two numbers.


while True:
	try:
	    yearInput = int(input("Please input a year: "))
	    if yearInput < 0:
	        print("You have inputted a negative number")
	    elif yearInput == 0:
	        print("You entered 0")
	    else:
	        if yearInput % 4 == 0:
	            print(f"{yearInput} is a leap year")
	            break
	        else:
	            print(f"{yearInput} is not a leap year")
	            
	except ValueError:
	    print("Invalid input: Please enter a valid positive year.")

print("-----Leap year check end here!-----")

rows = int(input("Enter number of rows: "))

columns = int(input("Enter number of columns: "))

i = 0
while i < rows:
	result = ""
	for x in range(columns):
		result  += "*"
		if x == rows:
			continue
	print(result)
	i += 1





# Stretch goal: Implement error checking for the leap year input
# 1. Strings are not allowed for inputs
# 2. No zero or negative values


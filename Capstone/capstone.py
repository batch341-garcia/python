# Capstone:
# Specifications
# 1. Create a Person class that is an abstract class that has the following methods
# 2. getFullName method
# b. addRequest method
# c. checkRequest method
# d. addUser method
from abc import ABC, abstractclassmethod

class Person(ABC):

	@abstractclassmethod
	def getFullName(self):
		pass

	@abstractclassmethod
	def addRequest(self):
		pass

	@abstractclassmethod
	def checkRequest(self):
		pass

	@abstractclassmethod
	def addUser(self):
		pass


# 2. Create an Employee class from Person with the following properties and methods
# 3. Properties(make sure they are private and have getters/setters)
# firstName, lastName, email, department
# b. Methods
# ‘Abstract methods
# For the checkRequest and addUser methods, they should do nothing.
# Jogin() - outputs “<Email> has logged in”
# Jogout() - outputs *<Email> has logged out”
# Note: All methods just return Strings of simple text
# ‘ex. Request has been added


class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def checkRequest(self):
		pass

	def addUser(self):
		pass

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		return "Request has been added"

	# getter
	def getFirstName(self):
		return self._firstName

	def getLastName(self):
		return self._lastName

	def getEmail(self):
		return self._email

	def getDepartment(self):
		return self._department

	# setter
	def setFirtNam(self, firstName):
		self._firstName = firstName

	def setLastName(self, lastName):
		self._lastName = lastName

	def setEmail(self, email):
		self._email = email

	def setDepartment(self, department):
		self._department = department

	def login(self):
		return f"{self._email} has logged in"
	def logout(self):
		return f"{self._email} has logged out"



# 3. Create a TeamLead class from Person with the following properties and methods
# 3. Properties(make sure they are private and have getters/setters)
# firstName, lastName, email, department, members
# b. Methods
# ‘Abstract methods
# For the addRequest and addUser methods, they should do nothing.
# Jogin() - outputs "<Email> has logged in”
# logout() - outputs "<Email> has logged out”
# addMember() - adds an employee to the members list
# Note: All methods just return Strings of simple text
# ex. Request has been added

class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._department = department
		self._members = []

	def checkRequest(self):
		return "Request has been added"

	def addUser(self):
		pass

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self, request):
		pass

	# getter
	def getFirstName(self):
		return self._firstName

	def getLastName(self):
		return self._lastName

	def getEmail(self):
		return self._email

	def getDepartment(self):
		return self._department

	def get_members(self):
		return self._members

	# setter
	def setFirtNam(self, firstName):
		self._firstName = firstName

	def setLastName(self, lastName):
		self._lastName = lastName

	def setEmail(self, email):
		self._email = email

	def setDepartment(self, email):
		self._email = email

	def setMembers(self, department):
		self._department = department

	def addMember(self, member):
		self._members.append(member)

	def login(self):
		return f"{self._email} has logged in"
	def logout(self):
		return f"{self._email} has logged out"




# 4. Create an Admin class from Person with the following properties and methods
# a. Properties(make sure they are private and have getters/setters)
# firstName, lastName, email, department
# b. Methods
# Abstract methods
# For the checkRequest and addRequest methods, they should do nothing.
# {ogin() - outputs "<Email> has logged in”
# Jogout() - outputs "<Email> has logged out"
# addUser() - outputs "New user added”
# Note: All methods just return Strings of simple text
# ex. Request has been added

class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department  = department

	def checkRequest(self):
		pass

	def addUser(self):
		return "User has been added"

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self, request):
		pass

	# getter
	def getFirstName(self):
		return self._firstName

	def getLastName(self):
		return self._lastName

	def getEmail(self):
		return self._email

	def getDepartment(self):
		return self._department

	# setter
	def setFirtNam(self, firstName):
		self._firstName = firstName

	def setLastName(self, lastName):
		self._lastName = lastName

	def setEmail(self, email):
		self._email = email

	def setDepartment(self, department):
		self._department = department

	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return "{self._email} has logged out"


# 5. Create @ Request that has the following properties and methods
# properties
# ame, requester, dateRequested, status
# b. Methods
# updateRequest
# closeRequest
# cancelRequest
# ‘Note: All methods just return Strings of simple text
# Ex. Request < name > has been updatediclosed/cancelled

class Request(Person):
	def __init__(self, name, requester, dateRequested, status="Pending"):
		super().__init__()
		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = status


	def checkRequest(self):
		pass

	def addUser(self):
		pass

	def getFullName(self):
		pass

	def addRequest(self, request):
		pass


	def updateRequest(self):
		return f"{self._name} has be updated"

	def closeRequest(self):
		return f"{self._name} has be closed"

	def cancelRequest(self):
		return f"{self._name} has be cancelled"


	# getter
	def get_name(self):
		return self._name

	def get_requester(self):
		return self._requester

	def get_date_requested(self):
 		return self._dateRequested

	def get_status(self):
 		return self._status

 	# setter

	def set_name(self, name):
		self._name = name

	def set_requester(self, requester):
		self._requester = requester

	def set_date_requested(self, dateRequested):
 		self._dateRequested = dateRequested

	def set_status(self, status):
 		self._status = status


employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")

employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")

employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")

employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")

admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")

teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")

req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")

req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"

assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"

assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"

assert employee2.login() == "sjane@mail.com has logged in"

assert employee2.addRequest() == "Request has been added"

assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)

teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():  
	print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")

print(req2.closeRequest())